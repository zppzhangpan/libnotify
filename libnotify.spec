Name:           libnotify
Version:        0.8.2
Release:        1
Summary:        Notification library on desktop
License:        LGPLv2+
URL:            http://www.gnome.org
Source0:        http://ftp.gnome.org/pub/GNOME/sources/libnotify/0.8/%{name}-%{version}.tar.xz
BuildRequires:  docbook-xsl-ns
BuildRequires:  glib2-devel >= 2.26.0 gi-docgen meson xmlto
BuildRequires:  gdk-pixbuf2-devel gtk3-devel gobject-introspection-devel
BuildRequires:  /usr/bin/xsltproc
Requires:       glib2 >= 2.26.0

%description
libnotify is a library for sending desktop notifications to a notification
daemon. These notifications can be used to inform the user about an event
or display some form of information without getting in the user's way.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and header files needed for development of programs using libnotify.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build

%install
%meson_install

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_bindir}/notify-send
%{_libdir}/libnotify.so.*
%{_libdir}/girepository-1.0/Notify-0.7.typelib
%{_mandir}/man1/notify-send.1*

%files          devel
%defattr(-,root,root)
%dir %{_includedir}/libnotify
%{_includedir}/libnotify/*
%{_libdir}/libnotify.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Notify-0.7.gir

%files          help
%defattr(-,root,root)
%doc NEWS
%doc %{_docdir}/libnotify/spec/*
%doc %{_datadir}/doc/libnotify-0/

%changelog
* Fri Jul 21 2023 zhangpan <zhangpan103@h-partners.com> - 0.8.2-1
- update to 0.8.2

* Tue Nov 1 2022 seuzw <930zhaowei@163.com> - 0.8.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 0.8.1

* Wed Jun 8 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.7.12-1
- Upgrade to 0.7.12

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.7.11-1
- Upgrade to 0.7.11

* Tue Jul 28 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 0.7.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 0.7.9

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.7.8-1
- Package init
